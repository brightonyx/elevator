<?php
/**
 * Different controller modes
 *
 * @author Paul Brighton <brighton@ieee.org>
 * @package enums
 */

namespace System\Enums;

use System\Enums\Base\Enum;

class ControllerMode extends Enum {
	const WEB = 'web';
	const API = 'api';
}
<?php
/**
 * Different date formats
 *
 * @author Paul Brighton <brighton@ieee.org>
 * @package enums
 */

namespace System\Enums;

use System\Enums\Base\Enum;

class Date extends Enum {
	const SYSTEMDATE    = 'Y-m-d';              // 2010-06-20
	const SYSTEMTIME    = 'H:i:s';              // 18:05:00
	const SYSTEMLOG     = 'Y-m-d H:i:s';        // 2010-06-20 18:05:00
}
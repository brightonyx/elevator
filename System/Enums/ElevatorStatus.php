<?php
/**
 * Different elevator statuses
 *
 * @author Paul Brighton <brighton@ieee.org>
 * @package enums
 */
namespace System\Enums;

use System\Enums\Base\Enum;

class ElevatorStatus extends Enum {
	const UP          = 'up';
	const DOWN        = 'down';
	const STAND       = 'stand';
	const MAINTENANCE = 'maintenance';
}
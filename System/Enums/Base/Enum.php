<?php
/**
 * Base enum class. To be filled out later
 *
 * @author Paul Brighton <brighton@ieee.org>
 * @package enums
 */

namespace System\Enums\Base;

abstract class Enum {

}
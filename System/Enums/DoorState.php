<?php
/**
 * Different door states
 *
 * @author Paul Brighton <brighton@ieee.org>
 * @package enums
 */
namespace System\Enums;

use System\Enums\Base\Enum;

class DoorState extends Enum {
	const OPEN        = 'open';
	const CLOSED      = 'closed';
	const JAMMED      = 'jammed';
	const MAINTENANCE = 'maintenance';
}
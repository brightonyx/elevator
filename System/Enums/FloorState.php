<?php
/**
 * Different floor states
 *
 * @author Paul Brighton <brighton@ieee.org>
 * @package enums
 */
namespace System\Enums;

use System\Enums\Base\Enum;

class FloorState extends Enum {
	const AVAILABLE   = 'available';
	const MAINTENANCE = 'maintenance';
}
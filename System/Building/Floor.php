<?php
/**
 * Floor class
 *
 * @author Paul Brighton <brighton@ieee.org>
 * @package building
 */

namespace System\Building;

use System\Enums\FloorState;

class Floor {
	/** @var FloorState|string $state Current state of the floor */
	private $state;

	/**
	 * Floor constructor
	 */
	public function __construct() {
		$this->state = FloorState::AVAILABLE;
	}

	/**
	 * Gets a current state
	 * @return FloorState|string
	 */
	public function get_state() {
		return $this->state;
	}

	/**
	 * Sets a state
	 *
	 * @param FloorState|string $state
	 */
	public function set_state($state) {
		$this->state = $state;
	}

	/**
	 * Checks if the floor is under maintenance
	 *
	 * @return bool
	 */
	public function in_maintenance(): bool {
		return $this->state == FloorState::MAINTENANCE;
	}
}
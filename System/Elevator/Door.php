<?php
/**
 * Door class
 *
 * @author Paul Brighton <brighton@ieee.org>
 * @package elevator
 */

namespace System\Elevator;

use System\Enums\DoorState;

class Door {
	/** @var DoorState|string Indicates the condition the door is currently in */
	private $state;

	/**
	 * Triggers alarm
	 */
	public function open() {
		if ($this->state == DoorState::CLOSED) {
			// Pass the signal to the actuators
			$this->state = DoorState::OPEN;
		}
	}

	/**
	 * Closes the doors
	 */
	public function close() {
		if ($this->state == DoorState::OPEN) {
			// Pass the signal to the actuators
			$this->state = DoorState::CLOSED;
		}
	}
}
<?php
/**
 * Elevator class with queuing logic
 *
 * @author Paul Brighton <brighton@ieee.org>
 * @package elevator
 */

namespace System\Elevator;

use System\Enums\ElevatorStatus;

class Elevator {
	/** @var array $queue List of floor requests currently queued. FIFO queue is used because a user who called the elevator first, should be looked after first */
	public $queue;
	
	/** @var ElevatorStatus $status What is the elevator currently doing */
	public $status;

	/** @var int $current_floor What floor the elevator is currently at */
	public $current_floor;

	/** @var string $id Unique elevator identifier */
	private $id = '';

	/** @var Door $door Instance of an elevator door */
	private $door = '';

	/**
	 * Elevators constructor. Assigns an elevator id and creates a door
	 *
	 * @param string $serial_number Serial number of this elevator
	 */
	public function __construct($serial_number = 'e1') {
		$this->id    = $serial_number;
		$this->door  = new Door();
		$this->queue = [];
	}
	
	/**
	 * Moves the actual elevator to a desired floor
	 *
	 * @param int $floor Destination
	 */
	public function send(int $floor) {
		// Sanity check
		if ($this->current_floor != $floor) {
			// Send a physical command to the motor
			// TODO as we pass floors, we need to update current_floor with the floor we are passing. This will determine its position in the shaft
		}
	}

	/**
	 * Triggers an alarm
	 */
	public function alarm() {
		$this->log('Alarm was activated');
		$this->flush_queue();
		$this->door_open(true);
		// Ring the alarm, call the fire department
	}

	/**
	 * Opens the doors
	 *
	 * @param bool $force In case we need to keep the door open
	 */
	public function door_open($force = false) {
		$this->door->open();
		if (!$force) {
			// Wait for few seconds before auto-closing the door
			sleep(2000);
			$this->door->close();
		}
	}

	/**
	 * Closes the doors
	 */
	public function door_close() {
		$this->door->close();
	}

	/**
	 * Listens for items in the elevator queue, and dispatches the elevator to the destination
	 */
	public function listen() {
		// Run until an error happens or somebody manually stops the operation of the elevator/controller
		//while(!$this->halt()) {
			// Read and remove the first item from the queue
			if (count($this->queue)) {
				$request     = array_shift($this_queue);
				$destination = key($request);
				$this->send($destination);
			}
			// If there are no more requests, send the elevator to the first floor
			else {
				$this->send(1);
			}
		//}
	}

	public function halt(): bool {
		$this->flush_queue();
		// Logic to stop the physical elevator operation
		return false;
	}


	/**
	 * Sends the elevator in a given status until it reaches a given floor
	 *
	 * @param ElevatorStatus|string $status Where the elevator needs to travel
	 * @param int                   $to Until what floor it needs to travel
	 * @return bool Success status
	 */
	public function add_to_queue(ElevatorStatus $status, int $to): bool {
		// Storing status in the parent property is a bit redundant
		$this->status  = $status;
		$this->queue[] = [$to => $status];
		// Send the command to the motor to ascent or descent this elevator. Return false if there was a problem
		return true;
	}

	/**
	 * Forces an item to the beginning of the queue. Used when a new destination is on the way
	 *
	 * @param ElevatorStatus|string $status Where the elevator needs to travel
	 * @param int                   $to Until what floor it needs to travel
	 * @return bool Success status
	 */
	public function add_to_queue_first(ElevatorStatus $status, int $to): bool {
		array_unshift($this->queue, [$to => $status]);
		return true;
	}

	/**
	 * Forces a destination as a second item in the queue. Used when a new destination is 'almost' on the way
	 *
	 * @param ElevatorStatus|string $status Where the elevator needs to travel
	 * @param int                   $to Until what floor it needs to travel
	 * @return bool Success status
	 */
	public function add_to_queue_second(ElevatorStatus $status, int $to): bool {
		$first_item = array_shift($this->queue);
		array_unshift($this->queue, [$to => $status]);
		array_unshift($this->queue, $first_item);
		return true;
	}

	/**
	 * Elevator reached the destination, we remove it from the queue
	 *
	 * @param int $floor
	 * @return bool Success status
	 */
	public function remove_from_queue(int $floor): bool {
		// Remove the first occurrence of the destination
		foreach ($this->queue as &$item) {
			if (key($item) == $floor) {
				unset($item);
				// TODO we can also remove all items that have this floor as a destination for cases when everybody gets off on the same floor
				break;
			}
		}
		return true;
	}

	/**
	 * Removes every item from the queue
	 *
	 * @return bool Success status
	 */
	private function flush_queue(): bool {
		$this->queue = [];
		return true;
	}

	/**
	 * Returns a current status of travel and a destination floor
	 *
	 * @return array
	 */
	public function get_current_operation(): array {
		// Remember, this is a FIFO queue - we always execute first request first
		return (count($this->queue))? $this->queue[0] : [];
	}

	/**
	 * Returns a last item in the queue
	 *
	 * @return array
	 */
	public function get_last_operation(): array {
		return (count($this->queue))? end($this->queue) : [];
	}

	/**
	 * Overwrites the controller log method by supplying a new file path for the log
	 *
	 * @param string $text
	 */
	private function log($text) {
		$file = 'elevator_' . $this->id . '.log';
		\Controller::log($text, $file);
	}
}
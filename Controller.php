<?php
/**
 * Main controller board that manages floors and elevators in the building
 *
 * @author Paul Brighton <brighton@ieee.org>
 * @package system
 */

// Autoload the namespaced classes
spl_autoload_register(function ($class) {
	$file = getcwd() . '/' . str_replace('\\', '/', $class) . '.php';
	if (file_exists($file)) {
		require $file;
	}
});

use System\Building\Floor;
use System\Elevator\Elevator;
use System\Enums\ControllerMode;
use System\Enums\Date;
use System\Enums\ElevatorStatus;
use System\Enums\FloorState;

class Controller {
	/** @var int NUMBER_OF_FLOORS Constant value of floors in this building */
	const NUMBER_OF_FLOORS = 10;

	/** @var int NUMBER_OF_ELEVATOR Constant value of elevators in this building */
	const NUMBER_OF_ELEVATORS = 3;

	/** @var Floor[] $floors List of all the floors in this building */
	private $floors;

	/** @var Elevator[] $elevators List of all elevators in this building */
	private $elevators;

	/** @var string $id Unique building identifier */
	private $building_id = '';

	/** @var int $runtime_id Each run will have its own runtime id */
	private static $runtime_id;

	/** @var string $mode The mode to run the app in - web or api */
	public static $mode = ControllerMode::WEB;

	/**
	 * Controller constructor. Assigns a runtime and building ids
	 *
	 * @param string $building_id
	 */
	public function __construct($building_id = 'c1') {
		self::$runtime_id  = time();
		$this->building_id = $building_id;
	}

	/**
	 * Powers up the system, creates floors and elevators
	 */
	public function power_up() {
		self::log('System is powering up');
		// For simplicity we will assume that floor numbers are numeric (in real life could be basement, parking, or PH)
		for ($i = 1; $i <= self::NUMBER_OF_FLOORS; $i++) {
			$this->floors[$i] = new Floor();
		}
		for ($i = 1; $i <= self::NUMBER_OF_ELEVATORS; $i++) {
			$this->elevators[$i] = new Elevator();
			// Listen for the queue commands
			$this->elevators[$i]->listen();
		}
	}

	/**
	 * Powers down the operations
	 */
	public function power_down() {
		self::log('System is powering down');
		$this->halt();
	}

	/**
	 * Shuts down each elevator
	 *
	 * @return bool
	 */
	public function halt(): bool {
		foreach ($this->elevators as $elevator) {
			$elevator->halt();
		}
		return false;
	}

	/**
	 * Finds the next available elevator. Listens for a request to go to a pickup floor and then to a destination.
	 * The end result of this method is to efficiently determine which elevator will handle the request by adding it to the queue
	 *
	 * @param int $to_floor The desired floor selected by the user
	 * @param int $from_floor The actual floor where user requested a lift. Defaults to the first floor
	 * @return string
	 * @throws Exception
	 */
	public function request_elevator(int $to_floor, int $from_floor = 1) {
		if (key_exists($to_floor, $this->floors)) {
			// Make sure the requested floor is not in maintenance
			if (!$this->floors[$to_floor]->in_maintenance()) {
				$this::log("Called to floor $to_floor");
				// Store the elevator number that will be the closest
				$selected_elevator = null;
				foreach ($this->elevators as $number => $elevator) {
					// First, check if the elevator is currently on the floor where the call came from. If so, load the passenger
					if ($elevator->current_floor == $from_floor) {
						$elevator->door_open();
						// Make sure the destination is on a different floor
						if ($from_floor != $to_floor) {
							$elevator->door_close();
							$elevator->add_to_queue(($from_floor > $to_floor)? ElevatorStatus::DOWN : ElevatorStatus::UP, $to_floor);
						}
						$selected_elevator = $number;
						break;
					}
					// Then check if the destination has been reached (keypads should disable current floor from being selected as destination)
					elseif ($elevator->current_floor == $to_floor) {
						$elevator->remove_from_queue($to_floor);
						$elevator->door_open();
					}
					// If we found an elevator that is standing on another floor, send it to where the request came from
					elseif ($elevator->status == ElevatorStatus::STAND) {
						$elevator->door_close();
						// We will need to add two items to the queue: from_floor and to_floor
						$elevator->add_to_queue(($elevator->current_floor > $from_floor)? ElevatorStatus::DOWN : ElevatorStatus::UP, $from_floor);
						$elevator->add_to_queue(($from_floor > $to_floor)? ElevatorStatus::DOWN : ElevatorStatus::UP, $to_floor);
						$selected_elevator = $number;
						break;
					}
					// If this elevator is already moving, see if it will be passing this floor on the way
					else {
						if ($request = $elevator->get_current_operation() and count($request)) {
							// See where the elevator is currently moving
							$destination = key($request);
							$direction   = $request[$destination];

							// We need to check two destinations: from_floor (to pick the user up) and to_floor. If both floors are on the way, no brainer
							if ($direction == ElevatorStatus::DOWN and $elevator->current_floor > $from_floor and $elevator->current_floor > $to_floor) {
								// Now we know it's on the way. Check if the to_floor happens before the requested destination
								if ($destination <= $from_floor and $destination <= $to_floor) {
									// If so, inject it as a first item in the queue to go there before the original destination
									$elevator->add_to_queue_first(ElevatorStatus::DOWN, $to_floor);
									$elevator->add_to_queue_first(ElevatorStatus::DOWN, $from_floor);
									$selected_elevator = $number;
									break;
								}
								// Otherwise add it as a second item to the queue since it is 'almost' on the way. TODO this needs more work to be efficient
								else {
									$elevator->add_to_queue_second(ElevatorStatus::DOWN, $to_floor);
									$elevator->add_to_queue_second(ElevatorStatus::DOWN, $from_floor);
									$selected_elevator = $number;
									break;
								}
							}
							// Apply the same logic if we're going up
							elseif ($direction == ElevatorStatus::UP and $elevator->current_floor < $from_floor and $elevator->current_floor < $to_floor) {
								// Now we know it's on the way. Check if the to_floor happens before the requested destination
								if ($destination >= $from_floor and $destination >= $to_floor) {
									// If so, inject it as a first item in the queue to go there before the original destination
									$elevator->add_to_queue_first(ElevatorStatus::UP, $to_floor);
									$elevator->add_to_queue_first(ElevatorStatus::UP, $from_floor);
									$selected_elevator = $number;
									break;
								}
								// Otherwise add it as a second item to the queue since it is 'almost' on the way. TODO this needs more work to be efficient
								else {
									$elevator->add_to_queue_second(ElevatorStatus::UP, $to_floor);
									$elevator->add_to_queue_second(ElevatorStatus::UP, $from_floor);
									$selected_elevator = $number;
									break;
								}
							}
						}
					}
				}
				// If no elevator was selected, add it toa queue of the least busiest elevator
				if (is_null($selected_elevator)) {
					// This is the easy part - if pickup or destination is not on the way, add it to the end of the queue of a random elevator.
					// TODO path optimization to find optimal/efficient route

					// We will use mt_rand because array_rand uses the libc generator, which is slower and less-random than Mersenne Twister
					$selected_elevator = mt_rand(1, self::NUMBER_OF_ELEVATORS);
					$elevator = $this->elevators[$selected_elevator];
					if ($request = $elevator->get_current_operation() and count($request)) {
						// See where the elevator is currently moving
						$destination = key($request);
						$direction   = $request[$destination];

						if ($direction == ElevatorStatus::DOWN and ($elevator->current_floor < $from_floor or $elevator->current_floor < $to_floor)) {
							// See where the last request will end up
							if ($last_request = $elevator->get_last_operation() and count($last_request)) {
								$last_destination = key($last_request);
								$elevator->add_to_queue(($last_destination > $from_floor)? ElevatorStatus::DOWN : ElevatorStatus::UP, $from_floor);
							} else {
								$elevator->add_to_queue(($elevator->current_floor > $from_floor)? ElevatorStatus::DOWN : ElevatorStatus::UP, $from_floor);
							}
							$elevator->add_to_queue(($from_floor > $to_floor)? ElevatorStatus::DOWN : ElevatorStatus::UP, $to_floor);
						}
						// Same thing for going up
						elseif ($direction == ElevatorStatus::UP and ($elevator->current_floor > $from_floor or $elevator->current_floor > $to_floor)) {
							// See where the last request will end up
							if ($last_request = $elevator->get_last_operation() and count($last_request)) {
								$last_destination = key($last_request);
								$elevator->add_to_queue(($last_destination > $from_floor)? ElevatorStatus::DOWN : ElevatorStatus::UP, $from_floor);
							} else {
								$elevator->add_to_queue(($elevator->current_floor > $from_floor)? ElevatorStatus::DOWN : ElevatorStatus::UP, $from_floor);
							}
							$elevator->add_to_queue(($from_floor > $to_floor)? ElevatorStatus::DOWN : ElevatorStatus::UP, $to_floor);
						}
					}
				}
				// If the elevator is still not selected at this point, something went wrong
				if (is_null($selected_elevator)) {
					throw new \Exception('Cannot add request to any queue');
				}

				// Let the UI know which elevator was selected
				return 'You are getting on elevator #' . $selected_elevator;
			} else {
				self::log("ERROR: Floor $to_floor is currently in maintenance");
			}
		} else {
			self::log("ERROR: Invalid floor selected: $to_floor");
		}
		return '';
	}

	/**
	 * Set a certain floor to be under maintenance
	 *
	 * @param int $floor_number
	 */
	public function floor_maintenance(int $floor_number) {
		// Make sure the floor number is valid
		if ($floor_number > 0 and $floor_number <= self::NUMBER_OF_FLOORS and key_exists($floor_number, $this->floors)) {
			$this->floors[$floor_number]->set_state(FloorState::MAINTENANCE);
		}
	}

	/**
	 * Writes a new entry to a log file for most of the actions including errors
	 *
	 * @param string $text Contents to write to a log file
	 * @param string $file File names can be given explicitly
	 */
	public static function log($text, $file = '') {
		$file = $file ?? 'runtime_' . self::$runtime_id . '.log';
		// Check the mode
		if (self::$mode == ControllerMode::WEB) {
			file_put_contents($file, date(Date::SYSTEMLOG) . " ::: $text" . PHP_EOL, FILE_APPEND);
		} else {
			// For API, simply return the output as JSON
			header('Content-Type: application/json');
			// Die on errors
			if (stripos($text, 'error') !== false) {
				die(json_encode(['text' => $text]));
			}
		}
	}
}

// Check if the form was submitted (POST).
// Coincidentally this is the same logic to listen for API requests
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	// Get the JSON input and decode
	$input = json_decode(file_get_contents("php://input"));
	// Pass the data to controller
	Controller::$mode = ControllerMode::API;
	$controller = new Controller('building1');

	try {
		$controller->power_up();
		//$controller->floor_maintenance(2);
		$result = $controller->request_elevator($input->to_floor, $input->from_floor);
		echo json_encode(['text' => $result]);
	} catch (\Exception $e) {
		Controller::log("Exception: $e");
		$controller->power_down();
	}
} else {
	header('Location: index.html');
}
